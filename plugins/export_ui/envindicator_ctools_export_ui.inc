<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'envindicator_environment',
  'access' => 'administer envindicator settings',

  // Define the menu item.
  'menu' => array(
    'menu prefix' => 'admin/config/user-interface',
    'menu item' => 'envindicator',
    'menu title' => 'Environment indicators',
    'menu description' => 'Administer environment indicators.',
  ),

  // Define user interface texts.
  'title singular' => t('environment'),
  'title plural' => t('environments'),
  'title singular proper' => t('Environment indicator'),
  'title plural proper' => t('Environment indicators'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'envindicator_ctools_export_ui_form',
    'submit' => 'envindicator_ctools_export_ui_form_submit',
  ),
);
