(function ($) {

  Drupal.behaviors.envindicatorToolbar = {
    attach: function (context, settings) {
      if (typeof(Drupal.settings.envindicator) != 'undefined') {
        var $name = $('<div>').addClass('envindicator-name-wrapper').html(Drupal.settings.envindicator['envindicator-name']);
        $('#toolbar div.toolbar-menu', context).once('envindicator').append($name);
        $('#toolbar div.toolbar-menu', context).css('background-color', Drupal.settings.envindicator['toolbar-color']);
        $('#toolbar div.toolbar-menu .item-list', context).css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.15, true));
        $('#toolbar div.toolbar-menu .item-list ul li:not(.envindicator-switcher) a', context).css('background-color', Drupal.settings.envindicator['toolbar-color']);
        $('#toolbar div.toolbar-drawer', context).css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.25));
        $('#toolbar div.toolbar-menu ul li a', context).hover(function () {
          $(this).css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.1));
        }, function () {
          $(this).css('background-color', Drupal.settings.envindicator['toolbar-color']);
          $('#toolbar div.toolbar-menu ul li.active-trail a', context).css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.1));
        });
        $('#toolbar div.toolbar-menu ul li.active-trail a', context).css('background-image', 'none').css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.1));
        $('#toolbar div.toolbar-drawer ul li a', context).hover(function () {
          $(this).css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.1, true));
        }, function () {
          $(this).css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.25));
          $('#toolbar div.toolbar-drawer ul li.active-trail a', context).css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.1, true));
        });
        $('#toolbar div.toolbar-drawer ul li.active-trail a', context).css('background-image', 'none').css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.1, true));
        // Move switcher bar to the top
        var $switcher = $('#toolbar .environment-switcher-container').parent().clone();
        $('#toolbar .environment-switcher-container').parent().remove();
        $('#toolbar').prepend($switcher);
      };
    }
  };
  
  Drupal.behaviors.envindicatorAdminMenu = {
    attach: function (context, settings) {
      if (typeof(Drupal.admin) != 'undefined') {
        // Add the restyling behavior to the admin menu behaviors.
        Drupal.admin.behaviors['envindicator'] = function (context, settings) {
          $('#admin-menu, #admin-menu-wrapper', context).css('background-color', Drupal.settings.envindicator['toolbar-color']);
          $('#admin-menu .item-list', context).css('background-color', changeColor(Drupal.settings.envindicator['toolbar-color'], 0.15, true));
          $('#admin-menu .item-list ul li:not(.environment-switcher) a', context).css('background-color', Drupal.settings.envindicator['toolbar-color']);
        };
      };
    }
  };

  Drupal.behaviors.envindicatorSwitcher = {
    attach: function (context, settings) {
      $('#envindicator .envindicator-name, #toolbar .envindicator-name-wrapper', context).live('click', function () {
        $('#envindicator .item-list, #toolbar .item-list', context).slideToggle('fast');
      });
    }
  }

})(jQuery);